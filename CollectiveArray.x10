import x10.array.*;
import x10.compiler.Inline;

public struct CollectiveArray[T] {
    val placeGroup:PlaceGroup;
    val arr:GlobalRail[T];
    val blockSize:Long;
    val home:Place;
    val placeRange:LongRange;
    val blockRange:LongRange;
    
    public def this(blockSize:Long) {T haszero} {
        this(PlaceGroup.WORLD, blockSize);
    }
    
    public def this(placeGroup:PlaceGroup, blockSize:Long) {T haszero} {
        this(placeGroup, blockSize, new Rail[T](blockSize * placeGroup.size()));
    }
    
    private def this(placeGroup:PlaceGroup, blockSize:Long, raw:Rail[T]) {
        this.placeGroup = placeGroup;
        this.blockSize = blockSize;
        this.arr = new GlobalRail[T](raw);
        this.home = here;
        this.placeRange = 0..(placeGroup.size() - 1);
        this.blockRange = 0..(blockSize - 1);
    }

    public def blockSize() = blockSize;
    
    public def asyncCopyFrom(src:Rail[T]) {
        assert src.size >= blockSize;
        Rail.asyncCopy(src, 0, arr, blockSize * placeGroup.indexOf(here), blockSize);
    }
    
    public def copyFrom(src:Rail[T]) {
        finish asyncCopyFrom(src);
    }
    
    public def allReduce(op:(T,T)=>T, unit:T) {
        if (here == arr.home) {
            for (idx in blockRange) {
                // reduce
                var v:T = unit;
                for (p in placeRange) {
                    v = op(v, arr(offset(p, idx)));
                }
                // broadcast
                for (p in placeRange) {
                    arr(offset(p, idx)) = v;
                }
            }
        }
    }
    
    public def allReduce(op:(T,T)=>T) {T haszero} {
        allReduce(op, Zero.get[T]());
    }
    
    @Inline private def xchange(id1:Long, id2:Long) {
        val v1 = arr(id1);
        val v2 = arr(id2);
        arr(id1) = v2;
        arr(id2) = v1;
    }
    
    /**
     * @return an IterationSpace containing all valid Points for indexing this Array.
     */  
    public def indices():DenseIterationSpace_2{self!=null} {
        return new DenseIterationSpace_2(0, 0, placeGroup.size()-1, blockSize-1);
    }

    @Inline public def offset(placeId:Long, idx:Long) {
        return blockSize * placeId + idx;
    }
    
    @Inline public def currentOffset(idx:Long) {
        return offset(currentId(), idx);
    }
    
    @Inline private def currentId() {
        return placeGroup.indexOf(here);
    }
    
    public def asyncAllToAll(dst:Rail[T]{self != null}, size:Long) {
        assert placeGroup.size * size <= dst.size;
        val currOffset = size * currentId();
        for (id in placeRange) {
            Rail.asyncCopy(arr, offset(id, currOffset), dst, size * id, size);
        }
    }
    public def allToAll(dst:Rail[T]{self != null}, size:Long) {
        finish asyncAllToAll(dst, size);
    }
    
    public def asyncCopyTo(dst:Rail[T]{self != null}) {
        Rail.asyncCopy(arr, currentOffset(0), dst, 0, blockSize);
    }
    
    public def copyTo(dst:Rail[T]{self != null}) {
        finish asyncCopyTo(dst);
    }
	
	public static def main(Rail[String]) {
	    val c = Clock.make();
	    val len = 8;
	    val cv = new CollectiveArray[Long](PlaceGroup.WORLD, len);
	    //val buff = new Rail[Long](3);
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
    	    val buff = new Rail[Long](len);
    	    val buff_out = new Rail[Long](len);
	        for (i in buff.range()) {
	            buff(i) = i + 10 * (here.id);
	        }
            if (here.id() == 1) Console.OUT.println(here + " = " + buff);
	        /*
	        buff(0) = p.id();
	        buff(1) = 1;*/
	        cv.copyFrom(buff);
	        c.advance();
	        cv.allToAll(buff_out, 2);
	        //cv.allreduce((x:Long,y:Long)=>x+y);
	        //c.advance();
	        if (here.id() == 1) Console.OUT.println(here + " = " + buff_out);
	    }
	    c.drop();
	}
}
