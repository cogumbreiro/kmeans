X10C ?= x10c
ARMUSC_JAR ?= ../armus-x10/target/armusc-x10.jar
JAVA ?= java
ARMUSC = $(JAVA) -jar $(ARMUSC_JAR)

all: KMeansSPMD.jar KMeansSPMD_SP.jar KMeansSPMDOrig.jar

%-C.jar: %.jar
	$(ARMUSC) $< $@

%.jar: %.x10
	$(X10C) -o $@ $<

clean:
	rm -f *.jar
