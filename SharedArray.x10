import x10.array.*;

public struct SharedArray[T] {
    val arr:Rail[Rail[T]];
    val blockSize:Long;
    val masterId:Long;
    
    public def this(masterId:Long, groupSize:Long, blockSize:Long) {T haszero} {
        this.masterId = masterId;
        this.blockSize = blockSize;
        this.arr = new Rail[Rail[T]](groupSize, (x:Long) => new Rail[T](blockSize));
    }
    
    public def this(masterId:Long, groupSize:Long, blockSize:Long, init:(Long)=>T) {
        this.masterId = masterId;
        this.blockSize = blockSize;
        this.arr = new Rail[Rail[T]](groupSize, (x:Long) => new Rail[T](blockSize, init));
    }
    
    public def blockSize() = blockSize;
    
    public def get(id:Long):Rail[T] {
        return arr(id);
    }
    
    public def allreduce(id:Long, op:(T,T)=>T, unit:T) {
        if (id == masterId) {
            for (idx in 0..(blockSize - 1)) {
                // reduce
                var v:T = unit;
                for (p in 0..(arr.size - 1)) {
                    v = op(v, arr(p)(idx));
                }
                // broadcast
                for (p in 0..(arr.size - 1)) {
                    arr(p)(idx) = v;
                }
            }
        }
    }
    
	public static def main(Rail[String]) {
	    val c = Clock.make();
	    val cv = new SharedArray[Long](0, Runtime.NTHREADS, 2);
	    for (id in 0..(Runtime.NTHREADS - 1)) async clocked(c) {
	        val buff = cv.get(id);
	        buff(0) = id;
	        buff(1) = 1;
	        c.advance();
	        cv.allreduce(id, (x:Long,y:Long)=>x+y, 0);
	        c.advance();
	        if (id == 0) Console.OUT.println(buff);
	    }
	    c.drop();
	}
}
