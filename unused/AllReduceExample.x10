import x10.array.*;
public class AllReduceExample {
	
	public static def printDistArray[T](array:DistArray[T]) {
		finish for (p in array.placeGroup()) async at (p) {
			for (idx in array.localIndices()) {
				val v = array(idx);
				Console.OUT.println(p + " " + idx + " -> " + v);
			}
		}
	}
  
	public static def main(Rail[String]) {
		// [5,1,2,3,7,8,4,2];
		val example = [187.31438,123.863106,69.03119,199.1375,199.17213,201.34225,263.99628,187.28642,50.31195];
		val red = new DistArray_Block_1[Double](example.size * Place.MAX_PLACES);
		val start = System.nanoTime();
		finish {
			val c = Clock.make();
			for (h in red.placeGroup()) at(h) async clocked(c) {
				val black = here.id() == 0 ? // [316,348,231,105] : [280,366,246,108];
						[187.31438,123.863106,69.03119,199.1375,199.17213,201.34225,263.99628,187.28642,50.31195] :
						[169.25272,120.46976,61.602665,170.2411,213.30145,197.99986,276.49933,206.80856,55.899242];
				Console.OUT.println(here + " -> " + black);
				SPMDUtil.allReduce(c, black, red, (x:Double,y:Double)=>x+y);
			}
			c.drop();
		}
		val elapsed = (System.nanoTime() - start)/1E9;
		Console.OUT.println("Elapsed: " + elapsed);
		if (here.id() == 0) {
			Console.OUT.println("Expected: " + [356.56708,244.33286,130.63385,369.3786,412.47357,399.3421,540.4956,394.09497,106.2112]);
			// [596,714,477,213]);
			Console.OUT.println("Obtained: " + red.raw());
		}
	}
}
