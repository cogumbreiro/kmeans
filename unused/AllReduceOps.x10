import x10.array.*;
public class AllReduceOps {

	private static def pointToRelativeIndex[T](arr:DistArray[T], pt:Point):Long {
		var result:Long = 0;
		for (idx in arr.localIndices()) {
			if (idx.equals(pt)) {
				return result;
			}
			result++;
		}
		return -1;
	}
  
	private static def relativeIndexToPoint[T](arr:DistArray[T], rel:Long): Point {
		assert rel >= 0;
		var result:Long = 0;
		for (idx in arr.localIndices()) {
			if (result == rel) {
				return idx;
			}
			result++;
		}
		return null;
	}
  
	private static def reduceOne[T](src:DistArray[T], dst:DistArray[T],
			reducer:(T,T)=>T, idx:Long) {
		for (h in src.placeGroup()) async at (h) {
			val srcPt = relativeIndexToPoint(src, idx);
			if (srcPt != null) {
				val v = src(srcPt);
				for (there in dst.placeGroup()) async at (there) {
					val dstPt = relativeIndexToPoint(dst, idx);
					if (dstPt != null) {
						atomic {
							dst(dstPt) = reducer(dst(dstPt), v);
						}
					}
				}
			}
		}
	}
  
	public static def allReduce[T](src:DistArray[T], dst:DistArray[T],
			reducer:(T,T)=>T) {
		for (pt in src.localIndices()) async {
			val idx = pointToRelativeIndex[T](src, pt);
			reduceOne(src, dst, reducer, idx);
		}
	}
	
	public static def allReduce[T](src:DistArray[T], dst:DistArray[T],
			reducer:(T,T)=>T, c:Clock) {
		val blockSize = (src.size / src.placeGroup().size()) as Long;
		for (h in src.placeGroup()) async clocked(c) at (h) {
			val offset = src.placeGroup().indexOf(h);
			// the number of phases
			for (i in 0..(blockSize - 1)) {
				val idx = (i + offset) % blockSize;
				val srcPt = relativeIndexToPoint(src, idx);
				if (srcPt != null) {
					val v = src(srcPt);
					for (there in dst.placeGroup()) async at (there) {
						val dstPt = relativeIndexToPoint(dst, idx);
						if (dstPt != null) {
							dst(dstPt) = reducer(dst(dstPt), v);
						}
					}
				}
				c.advance();
			}
		}
	}
	
	public static def allReduceClocked[T](src:DistArray[T], dst:DistArray[T],
			reducer:(T,T)=>T) {
		val c = Clock.make();
		allReduce(src, dst, reducer, c);
		c.drop();
	}
}
