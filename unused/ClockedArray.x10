import x10.array.*;

public struct ClockedArray[T] {
    val arr: Rail[ClockedValue[T]];
    val blockSize:Long;
    
    public def this(placeGroup:PlaceGroup, blockSize:Long) {T haszero} {
        this.blockSize = blockSize;
        this.arr = new Rail[ClockedValue[T]](blockSize, (idx:Long) => {
            val place:Place = placeGroup(placeGroup.indexOf(idx % placeGroup.size()));
            return at(place) new ClockedValue[T](placeGroup);
        });
    }
    
    public def set(src:Rail[T]) {
	    finish for (idx in src.range()) {
	        arr(idx).set(src(idx));
	    }
    }
    
    public def compute(op:(T,T)=>T, unit:T) {
	    finish for (idx in 0..(blockSize-1)) {
	        arr(idx).compute(op, unit);
	    }
    }
    
    public def get(dst:Rail[T]) {
	    finish for (idx in dst.range()) {
	        arr(idx).get((v:T) => {
	            dst(idx) = v;
            });
	    }
    }
	
	public static def main(Rail[String]) {
	    val c = Clock.make();
	    val cv = new ClockedArray[Long](PlaceGroup.WORLD, 2);
	    val buff = new Rail[Long](2);
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
	        buff(0) = p.id();
	        buff(1) = 1;
	        cv.set(buff);
	        c.advance();
	        cv.compute((x:Long,y:Long)=>x+y, 0);
	        c.advance();
	        cv.get(buff);
	        if (here.id() == 0) Console.OUT.println(buff);
	    }
	    c.drop();
	}
}
