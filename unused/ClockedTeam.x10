import x10.array.*;

public struct ClockedTeam[T] {
    val arr: DistArray[T];
    val blockSize:Long;
    val clock:Clock;
    
    public def this(c:Clock, blockSize:Long) {T haszero} {
        this.blockSize = blockSize;
        this.arr = new DistArray_Block_1[T](blockSize*Place.MAX_PLACES);
        this.clock = c;
    }
	public def reduce(src:Rail[T], reducer:(T,T)=>T) {
		val offset = arr.placeGroup().indexOf(here);
		// the number of phases
		for (i in 0..(blockSize - 1)) {
			val idx = (i + offset) % blockSize;
			val v = src(idx);
			finish for (there in arr.placeGroup()) async at (there) {
				arr.raw()(idx) = reducer(arr.raw()(idx), v);
			}
			clock.advance();
		}
	}
	
	public def allReduce(src:Rail[T], reducer:(T,T)=>T, unit:T) {
	    assert src.size == blockSize;
    	SPMDUtil.fill(arr, unit);
		clock.advance();
        reduce(src, reducer);
    	SPMDUtil.copy(arr, src);
	}

	public def allReduce(src:Rail[T], reducer:(T,T)=>T) {T haszero} {
	    allReduce(src, reducer, Zero.get[T]());
	}
}
