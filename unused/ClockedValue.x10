import x10.array.*;
import x10.util.*;

public struct ClockedValue[T] {
    val placeGroup:PlaceGroup;
    val buffer:GlobalRail[T];
    val home:Place;
    
    public def this(placeGroup:PlaceGroup) {T haszero} {
        this.placeGroup = placeGroup;
        this.buffer = new GlobalRail[T](new Rail[T](placeGroup.size()));
        this.home = here;
    }
    
    public def set(datum:T) {
        val idx = placeGroup.indexOf(here);
        if (here == buffer.home()) {
            buffer(idx) = datum;
        } else {
            at(buffer.home()) async {
                buffer(idx) = datum;
            }
        }
    }
    
    public def compute(op:(T,T)=>T, unit:T) {
        if (here == buffer.home()) {
            buffer(0) = RailUtils.reduce(buffer(), op, unit);
        }
    }
    
    public def get(handler:(T) => void) {
        if (here == buffer.home()) {
            handler(buffer(0));
        } else {
            async {
                val v = at(buffer.home()) buffer(0);
                handler(v);
            }
        }
    }
    
	public static def main(Rail[String]) {
	    val c = Clock.make();
	    val cv = new ClockedValue[Long](PlaceGroup.WORLD);
	    for (p in PlaceGroup.WORLD) at(p) async clocked(c) {
	        cv.set(p.id());
	        c.advance();
	        if (here == cv.home) Console.OUT.println(cv.buffer());
	        cv.compute((x:Long, y:Long)=>x+y, 0);
	        c.advance();
	        cv.get((x:Long) => {Console.OUT.println(x);});
	    }
	    c.drop();
	}
}
