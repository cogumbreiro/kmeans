public interface SPMDArray[T] {
    def blockSize():Long;
    def asyncWrite(src:Rail[T]{self != null}):void;
    def allreduce(op:(T,T)=>T, unit:T):void;
    def asyncRead(dst:Rail[T]{self != null}):void;
}
