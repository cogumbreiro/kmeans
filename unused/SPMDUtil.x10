import x10.array.*;
public class SPMDUtil {

	public static def pointToRelativeIndex[T](arr:DistArray[T], pt:Point):Long {
		var result:Long = 0;
		for (idx in arr.localIndices()) {
			if (idx.equals(pt)) {
				return result;
			}
			result++;
		}
		return -1;
	}
  
	public static def relativeIndexToPoint[T](arr:DistArray[T], rel:Long): Point {
		assert rel >= 0;
		var result:Long = 0;
		for (idx in arr.localIndices()) {
			if (result == rel) {
				return idx;
			}
			result++;
		}
		return null;
	}

	public static def allReduce[T](c:Clock, src:Rail[T], dst:DistArray[T],
			reducer:(T,T)=>T) {
		val blockSize = src.size; 
		val offset = dst.placeGroup().indexOf(here);
		// the number of phases
		for (i in 0..(blockSize - 1)) {
			val idx = (i + offset) % blockSize;
			if (idx < src.size) {
				val v = src(idx);
				finish for (there in dst.placeGroup()) async at (there) {
					val dstPt = relativeIndexToPoint(dst, idx);
					if (dstPt != null) {
						dst(dstPt) = reducer(dst(dstPt), v);
					}
				}
			}
			c.advance();
		}
	}

	public static def allReduceCopy[T](c:Clock, src:Rail[T], dst:DistArray[T],
			reducer:(T,T)=>T) {
		allReduce(c, src, dst, reducer);
		copy(dst, src);
	}	
	
	public static def copy[T](src:DistArray[T], dst:DistArray[T]) {
        Rail.copy(src.raw(), dst.raw());
	}
	
	public static def copy[T](src:DistArray[T], dst:Rail[T]) {
        Rail.copy(src.raw(), dst);
	}
	public static def copy[T](src:Rail[T], dst:DistArray[T]) {
        Rail.copy(src, dst.raw());
	}
	
	public static def clear[T](arr:DistArray[T]) {T haszero} {
		arr.raw().clear();
	}

	public static def fill[T](arr:DistArray[T], unit:T) {
		arr.raw().fill(unit);
	}
}
